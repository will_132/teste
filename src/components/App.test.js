import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';

import { Route , MemoryRouter } from 'react-router-dom';
//import do enzyme
import { shallow, configure, mount } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

import Inicio from './Inicio';
import Contato from './Contato';
import Sobre from './Sobre';
import { tsExternalModuleReference } from '@babel/types';

configure({ adapter: new Adapter() });

let pathMap = {};

describe('Testando rotas da App', () => {
    beforeAll(() => { 
        const component = shallow(<App />);
        pathMap = component.find(Route).reduce((pathMap, route) => {
            const routeProps = route.props();
            pathMap[routeProps.path] = routeProps.component;
            return pathMap;
        },{});

    })
    it('/inicio carrega o componente inicio', () =>{
        expect(pathMap['/inicio']).toBe(Inicio);
    })

    it('/sobre carrega o componente sobre', () =>{
        expect(pathMap['/sobre']).toBe(Sobre);
    })

    it('/contato carrega o componente contato', () =>{
        expect(pathMap['/contato']).toBe(Contato);
    })

    test ('/Inicio utilizando Memory Route', () => {
        const envelope = mount(
            <MemoryRouter inicialEntries = {['/Inicio']}> <App/> </MemoryRouter>
        );
        expect (envelope.find(Inicio)).toHaveLength(1);
    })
});
