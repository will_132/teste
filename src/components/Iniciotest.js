import React from 'react';
import Inicio from './Inicio';

//Utilizado para configuração do Enzyme
import { shallow, configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
 '@babel/types';

configure ({adapter: new Adapter()});
describe ("Teste de renderização do campo Inicio", () =>{
    it("Verificar se a pagina inicial e carregada adequadamente",() =>{
        const wrapper = shallow (<Inicio/>);
        expect(wrapper).toMatchSnapshot();
    });

});